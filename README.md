# CI/CD components for CMake

This project provides
[CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html)
pipeline for a software project built using [CMake](https://cmake.org/).

[[_TOC_]]

## Usage

You can add the individual component templates to an existing `.gitlab-ci.yml`
file by using the `include:` keyword.

For more details see the online documentation at

<https://computing.docs.ligo.org/guide/gitlab/components/cmake/>

## Contributing

Please read about CI/CD components and best practices at:
<https://git.ligo.org/help/ci/components/index.html>.

All interactions related to this project should follow the
[LIGO-Virgo-KAGRA Code of Conduct](https://dcc.ligo.org/LIGO-M1900037).

For more details on contributing to this project, see `CONTRIBUTING.md`.
