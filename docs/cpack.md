# `cmake/cpack`

Configure a job to build a source distribution for a CMake project.

## Description

This components creates a job that uses
[CPack](https://cmake.org/cmake/help/latest/module/CPack.html)
to create a source distribution for a project.

## Usage

```yaml
includes:
  - component: git.ligo.org/computing/gitlab/components/cmake/cpack@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `cpack` | The name to give the job. |
| `image` | REQUIRED { .nowrap } | Container image in which to build the sdist |
| `stage` | `build` | The pipeline stage to add jobs to |
| `before_script` | `[]` | `before_script` commands to run to install requirements |
| `project_dir` | `"."` | CMake project root directory (containing `CMakeLists.txt`) |
| `cmake_options` | `""` | Extra options to pass to the `cmake` configure command |
| `source_distribution_name` { .nowrap } | `"*.tar."` | Name/pattern to identify the source distribution |

## Notes

### Configuration requirements need to be preinstalled { #requirements }

This component makes no attempt to install any packages except `cmake`, so
the user must ensure that any requirements are manually installed.

The authors recommend one of the following strategies

1.  Create a container image that bundles all of the relevant requirements,
    including `cmake`, and pass that to the `image` input.

2.  Use the `before_script` input to install the necessary requirements into
    a software environment.

    With this strategy, it may be useful to use one of the
    [`igwn/builder`](https://hub.docker.com/r/igwn/builder) images, which
    come with many standard IGWN build tools preinstalled.

    See the examples below for more information.

## Examples

### Build a source distribution using a specialist container image { #example-image }

!!! example "Build a source distribution using a specialist container image"

    ```yaml
    includes:
      - component: git.ligo.org/computing/gitlab/components/cmake/cpack@<VERSION>
        inputs:
          image: containers.ligo.org/mygroup/myproject-image:latest
    ```

### Build a source distribution using a `before_script` { #example-before_script }

!!! example "Build a source distribution using `before_script`"

    ```yaml
    includes:
      - component: git.ligo.org/computing/gitlab/components/cmake/cpack@<VERSION>
        inputs:
          image: igwn/builder:el9-testing
          before_script:
            - dnf -y -q install
                cmake
                gcc
                librequirement1
                pkg-config
                tool2
    ```
