# CMake CI/CD components

The `computing/gitlab/components/cmake` project provides
[CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
for a software project built using [CMake](https://cmake.org/).

## Latest release

[![latest badge](https://git.ligo.org/computing/gitlab/components/cmake/-/badges/release.svg?text=test)](https://git.ligo.org/explore/catalog/computing/gitlab/components/cmake/ "See latest release in the CI/CD catalog")

## Components

The following components are available:

- [`cmake/cpack`](./cpack.md "`cmake/cpack` component documentation")
